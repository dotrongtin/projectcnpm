# ProjectCNPM
Name: Đỗ Trọng Tín
MSSV: 2180608101
Class: 21DTHD4

| Title               | Manager monthly sales statistics |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want a monthly sales statistics|
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager entered a specific month and year<br>Then the desktop will show the statistic that the month and year similar to the input<br>And ensure that the chosen month and year statistic is calculated correctly |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner               | Đỗ Trọng Tín|
| Interation          | Unschedules|

| Title               | Manager add employee profile |
|---------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to add an employee profile |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager entered an employee profile<br>Then the desktop will add the input into database<br>And ensure that display the added profile<br>Acceptance Criteria 2:<br>Given that manager select a file of employee list<br>Then the desktop will add the data in the file into the database<br>And ensure that display the added profile |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |

| Title               | Manager delete employee profile |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to delete an employee profile |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager choose an employee profile<br>Then the desktop will remove the chosen one out from database<br>And ensure that display the after remove profile list |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |

| Title               | Manager edit employee profile |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to edit an employee profile |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager edit an employee profile that he choose<br>Then the desktop will rewrite the chosen data in the database<br>And ensure that display the after edit profile list |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |

| Title               | Manager add supplier information |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to add a supplier information |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager entered a supplier information<br>Then the desktop will add the input into the database<br>And ensure that display the added information |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |

| Title               | Manager delete supplier information |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to delete a supplier information |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager choose a supplier information<br>Then the desktop will remove the chosen data in the database<br>And ensure that display the after delete information list |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |   

| Title               | Manager edit supplier information |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement     | As a manager I want to edit a supplier information |
| Acceptance Criteria | Acceptance Criteria 1:<br>Given that manager edit a supplier information that he choose<br>Then the desktop will rewrite the chosen data in the database<br>And ensure that display the after edit information list |
| Definition of done  | Unit Test Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Test Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story |
| Owner               | Đỗ Trọng Tín |
| Interation          | Unschedules |